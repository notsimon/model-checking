Il s'agit d'écrire un petit model checker CTL utilisant des BDDs.

L'original de ce sujet est ici :

  http://www.lrde.epita.fr/~adl/ens/mc/2012/projet.txt


Modalités
=========

 - À faire par groupe de deux.

 - Inscription : Une fois que vous avez trouvé votre complice, envoyez
      1) vos deux noms, et
      2) vos deux adresses email
   à <aduret+ctl@gmail.com> *avant* le lundi 28 mai midi,
   dans un mail ayant pour sujet "inscription projet IMC".

 - Rendu : Envoyez une tarball à <aduret+ctl@gmail.com)> au plus tard
   le dimanche 10 juin dans un mail ayant pour sujet "rendu projet IMC".
   Nommez votre tarball avec les deux logins du groupe, dans l'ordre
   alphabetique.

Je répondrai à chaque mail pour confirmer, et la liste des groupes
inscrits sera mise à jour ici :

  http://www.lrde.epita.fr/~adl/ens/mc/2012/groupes.org

Si rien ne bouge, relancez moi.


Travail à effectuer
===================

Écrire un programme prenant exactement deux arguments, dans cet ordre :
   - un nom de fichier contenant une structure de Kripke
   - une formule CTL
Le programme devra indiquer si la structure de Kripke
satisfait la formule CTL : un simple oui/non suffit.

Libre à vous d'ajouter des options (pour débugger, par exemple).

En pratique, les étapes importantes de votre programme sont
  - lire la structure de Kripke
  - la représenter en mémoire la relation de transition sous forme de BDD
    (cela peut se faire à la volée lors de la lecture)
  - lire la formule CTL
  - interpréter la formule CTL sur la structure de Kripke à l'aide
    d'algorithmes ensemblistes manipulant les BDDs.


Pour ne pas y passer trop de temps on vous suggère d'utiliser une
bibliothèque BDD existante telle que BuDDy, CUDD, Jinc, ou une autre.

  http://sourceforge.net/projects/buddy
  http://vlsi.colorado.edu/~fabio/CUDD
  http://www.jossowski.de/projects/jinc/jinc.html


Données de test
===============

Le fichier http://www.lrde.epita.fr/~adl/ens/mc/2012/projet/kripke.tgz
contient Quatre structures de Kripke générées à partir de modèles
Promela.  (Ces modèles sont dans le répertoire bench/emptchk/models/
de la distribution de Spot si cela vous intéresse.)

  cl3serv1.kripke (510 états)
  cl3serv3.kripke (12412 états)

    Sont des exemples communication de client/serveur.
      "w1" indique que le premier client attend après une requête
      "s1" indique que le premier client est servi

    Vous pourriez essayer de regarder si AG(IMPLIES(w1,AF(s1))),
    c'est-à-dire qu'un client en attente finira par être servi.
    Je ne vous donne pas la réponse, parce que je suis méchant.

  eeaean1.kripke (49332 états)
  eeaean2.kripke (145400 états)

    Sont des variations d'un protocole d'élection de leader.
    Les propositions atomiques indiquent qui est le leader.

    Vous pourriez essayer de vérifier des formules telles que
      EF(threeLeads)
      EU(noLeader, zeroLeads)
      AG(IMPLIES(noLeader,AF(zeroLeads)))
    etc.

Bien entendu on vous recommande de faire vos propre petites
structures de Kripke pour débugger vos algorithmes ; celles-ci
sont un peu trop grosses pour être interprétées à la main...


Formats
=======

Les structure de Kripke sont écrites dans le format suivant :

   510               <- nombre d'états (numérotés de 0 à 509 dans ce cas)
   0 !w1 s1          <- description de l'état 0: w1 est faux, s1 est vrai
    1 362 507        <- liste des états successeurs de l'état 0
   1 !w1 s1          <- description de l'état 1: w1 est faux, s1 est vrai
    2 317 504        <- liste des états successeurs de l'état 0
   [...]
   509 w1 !s1        <- description de l'état 509: w1 est vrai, s1 est faux
    419 506 391      <- liste des états successeurs de l'état 509

Il y a toujours au moins un état, et l'état initial est toujours l'état 0.

Si un état (compris entre 0 et le nombre maximal indiqué en haut)
n'apparaît pas dans le fichier, c'est qu'il n'a pas de successeurs.
Vous pouvez considérer que son étiquette est "false".

Vous avez le droit d'écrire un convertisseur de format.  Si ce format
vous semble trop lent à parser.  Dans ce cas,

Votre parseur de formules CTL doit reconnaître au moins les opérateurs
suivants :

   AND(f,g), OR(f,g), NOT(f), IMPLIES(f,g)
   AX(f), EX(f), AF(f), EF(f), AG(f), EG(f), AU(f,g), EU(f,g),
   true, false

Vous pouvez changer la syntaxe de ces formules, à condition de ne pas
utiliser une notation postfixée (polonaise inverse), ou préfixée sans
les parenthèses.  Dans ce cas, donnez moi tous les exemples ci-dessus
dans votre syntaxe.


Contenu de la tarball
=====================

  - un fichier AUTHORS

  - un fichier README expliquant comment compiler (la machine de test
     Debian 64bit)
    et lancer votre outil sur les exemples ci-dessus.  Je veux être
    capable de tester sur d'autres exemples aussi.

  - tous vos sources

  - aucun fichier binaire

Vous pouvez supposer que les trois bibliothèques BDD ci-dessus sont
installées dans /usr/local/.


Barème (indicatif)
==================

  - Le package est bien emballé, documenté => 2pts
  - Le code est propre, lisible, on s'y retrouve facilement => 2pts
  - Il y a une suite de tests raisonnable => 2pts
  - Les fonctionnalités demandés sont correctement implémentées => 5pts
  - Ça marche (sans tricher) sur les exemples => 4pts
  - C'est rapide (par rapport aux autres) => 2pts
  - Bonus (multi-cœur, techniques de reductions, etc.) => 3 pts

Malus:
  - Je dois vous envoyer un mail parce que je n'arrive pas à
    compiler/utiliser votre outil après avoir lu le README => -2
  - Ne s'est pas inscrit avant le lundi 28 mai midi => -25% de la note
  - Ne s'est pas inscrit avant fin mai => -100% de la note.  (En clair : 0.)
  - N'a pas rendu avant le 10 juin minuit => -33% par jour de retard entamé.


Questions ?
===========

À poser en cours, ou par mail avant le 28 mai.


Nota Bene
=========

Ce projet n'est pas compliqué et je vous recommande de vous en
débarrasser au plus tôt.  Les deadlines fin mai et début juin ne sont
là que pour vous donner l'occasion de vous y prendre au dernier moment
(quand il n'est plus possible de poser des questions), voire de
complètement oublier (ce qui me fait moins de correction).
