#include <string>
#include <iostream>
#include <utility>
#include <fstream>
#include <vector>
#include <map>

#include "kripke-parser.hh"

void KripkeBDD::init_bdd(int nb_nodes) {
	int domain[] = { nb_nodes, nb_nodes };
	fdd_extdomain(domain, 2);
	offset_ = bdd_varnum();
	max_ = nb_nodes;
}

void KripkeBDD::insert_node(Node& n) {
	for (int dest: n.links)
		set_relation(n.num, dest);

	set_labels(n.num, n.vars);
}

void KripkeBDD::set_relation(int a, int b) {
	if (a < max_ && b < max_)
		R_ |= (fdd_ithvar(0, a) & fdd_ithvar(1, b));
	else
		std::cerr << "Range excessed (" << a << ", "
				  << b << "). Skipped" << std::endl;
}

void KripkeBDD::set_labels(int node, std::vector<std::string>& labels) {
	bdd vbdd = bddtrue;

	for (std::string& s : labels) {
		std::string var = (s[0] == '!') ? s.substr(1) : s;
		if (labels_.find(var) == labels_.end()) {
			bdd_extvarnum(1);
			labels_.insert(
				std::pair<std::string, int>(var, offset_ + labels_.size())
			);
		}
		int i = KripkeBDD::labels_[var];
		vbdd &= (s[0] != '!') ? bdd_ithvar(i) : bdd_nithvar(i);
	}

	if (node < max_)
		L_ |= (fdd_ithvar(0, node) & vbdd);
	else
		std::cerr << "Range excessed (" << node << "). Skipped" << std::endl;
}

bdd KripkeBDD::remove_labels(const bdd L) {
	int vars[labels_.size()];
	int i = 0;
	for (auto& p: labels_)
		vars[i++] = p.second;

	bdd labels = bdd_makeset(vars, labels_.size());
	return bdd_exist(L, labels);
}

void KripkeBDD::simplify() {
	bdd tmp;

	// Delete unreachable node
	/*bddPair *toSrc = bdd_newpair();
	fdd_setpair(toSrc, 1, 0);
	bdd res = R_ & fdd_ithvar(0, 0);
	bdd labels = fdd_ithvar(0, 0);
	labels |= bdd_replace(bdd_exist(res, fdd_ithset(0)), toSrc);
	
	do {
		tmp = res;
		res |= R_ & bdd_replace(bdd_exist(res, fdd_ithset(0)), toSrc);
		labels |= bdd_replace(bdd_exist(res, fdd_ithset(0)), toSrc);
	}
	while (res != tmp);

	bdd_freepair(toSrc);
	L_ &= labels;
	R_ = res;*/

	// Delete finite paths
	bddPair* to_dest = bdd_newpair();
	fdd_setpair(to_dest, 0, 1);
	
	bdd nodes = remove_labels(L_);
	bdd F;
	do {
		tmp = R_;
		F = nodes - bdd_exist(R_, fdd_ithset(1)); // non final states are not origins of links
		nodes -= F; // remove finals from nodes
		R_ -= R_ & bdd_replace(F, to_dest); // remove the link with a final state as destination
	} while (tmp != R_);

	L_ &= nodes; // restrict L_ to non-final nodes
}
