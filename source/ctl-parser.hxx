#ifndef CTL_PARSER_HXX
# define CTL_PARSER_HXX

#include <boost/spirit/include/qi_no_case.hpp>

template <typename Iterator>
CTLFormula<Iterator>::CTLFormula ()
	: CTLFormula::base_type(expr) {
	//using boost::spirit::ascii::no_case;

	expr = unary_op	
		 | binary_op
		 | qi::bool_
		 | id;

	unary_op = (qi::no_case[unary_symbol] >> '(' >> expr >> ')');
	binary_op = (qi::no_case[binary_symbol] >> '(' >> expr >> ',' >> expr >> ')');
	id = (qi::char_("a-zA-Z") >> *qi::char_("a-zA-Z0-9_-"));

#ifndef NDEBUG
	expr.name("expr");
	unary_op.name("unary_op");
	binary_op.name("binary_op");
	//debug(expr);
	//debug(unary_op);
	//debug(binary_op);
#endif
}

#endif
