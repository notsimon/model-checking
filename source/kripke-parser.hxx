#ifndef KRIPKE_PARSER_HXX
# define KRIPKE_PARSER_HXX

template <typename Iterator>
KripkeFile<Iterator>::KripkeFile (KripkeBDD& skripke)
	: KripkeFile::base_type(file)
	, kripke(skripke)
{

	file = qi::int_					[phoenix::bind(&KripkeBDD::init_bdd, kripke, qi::_1)]	
		>> *(
				qi::eol
			 >> node				[phoenix::bind(&KripkeBDD::insert_node, kripke, qi::_1)]
			);

	node = qi::int_					[phoenix::at_c<0>(qi::_val) = qi::_1]
		>> -(		+qi::space
				>>	values			[phoenix::at_c<1>(qi::_val) = qi::_1])
		>> -(
					qi::eol
				>>	+qi::space
				>>	links			[phoenix::at_c<2>(qi::_val) = qi::_1]
			);

	values = (id % ' ');

	links = (qi::int_ % ' ');

	id = (-qi::char_("!") >> (qi::char_("a-zA-Z"))) >> *qi::char_("a-zA-Z0-9_-");
}

#endif
