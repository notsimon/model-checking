#include <iostream>
#include <cassert>
#include "ctl-eval.hh"

bdd CTLEval::operator()(bool b) {
	return b ? bddtrue : bddfalse;
}

bdd CTLEval::operator()(std::string& id) {
	auto& labels = kripke_.labels_get();
	if (labels.find(id) == labels.end()) {
		std::cerr << "Unknown variable " << id << std::endl;
		assert(false);
	}

	bdd res = kripke_.L_get() & bdd_ithvar(kripke_.labels_get()[id]);

	return constraint(res);
}

bdd CTLEval::EX(bdd expr) {
	bddPair *p = bdd_newpair();
	fdd_setpair(p, 0, 1);
	expr = bdd_replace(expr, p);
	bdd_freepair(p);
	return constraint(kripke_.R_get() & expr);
}

bdd CTLEval::AX(bdd expr) {
	return NOT(EX(NOT(expr)));
}

bdd CTLEval::NOT(bdd expr) {
	return constraint(!expr);
}

bdd CTLEval::operator()(UnaryOp& node) {
	bdd expr = boost::apply_visitor((*this), node.expr);
	switch (node.op) {
		case UnarySymbol::NOT:
			return NOT(expr);

		// Every succ of the node respect expr
		case UnarySymbol::AX:
			return AX(expr);

		// At least one succ of the node respects expr
		case UnarySymbol::EX:
			return EX(expr);

		case UnarySymbol::AF:
		{
			bdd res = constraint(kripke_.R_get() & expr);
			bddPair* p = bdd_newpair();
			fdd_setpair(p, 0, 1);

			bdd tmp;
			do {
				tmp = res;
				res |= AX(res);
			}
			while (tmp != res);

			bdd_freepair(p);
			return res;
		}

		case UnarySymbol::EF:
		{
			bdd res = constraint(kripke_.R_get() & expr);

			bdd tmp;
			do {
				tmp = res;
				res |= EX(res);
			}
			while (tmp != res);

			return res;
		}

		case UnarySymbol::AG:
		{
			bdd res = constraint(kripke_.R_get() & expr);

			bdd tmp;
			do {
				tmp = res;
				res &= AX(res);
			}
			while (tmp != res);

			return res;
		}

		case UnarySymbol::EG:
		{
			bdd res = constraint(kripke_.R_get() & expr);

			bdd tmp;
			do {
				tmp = res;
				res &= EX(res);
			}
			while (tmp != res);

			return res;
		}
	}

	return bddfalse;
}

bdd CTLEval::operator()(BinaryOp& node) {
	bdd left = boost::apply_visitor((*this), node.left);
	bdd right = boost::apply_visitor((*this), node.right);
	switch (node.op) {
		case BinarySymbol::AND:
			return constraint(left & right);

		case BinarySymbol::OR:
			return constraint(left | right);

		case BinarySymbol::IMPLIES:
			return constraint(left >> right);

		case BinarySymbol::AU:
		{
			bdd res = constraint(kripke_.R_get() & right);

			bdd tmp;
			do {
				tmp = res;
				res |= (AX(res) & left);
			}
			while (tmp != res);

			return constraint(kripke_.R_get() & right) | res;
		}

		case BinarySymbol::EU:
		{
			bdd res = constraint(kripke_.R_get() & right);

			bdd tmp;
			do {
				tmp = res;
				res |= (EX(res) & left);
			}
			while (tmp != res);

			return constraint(kripke_.R_get() & right) | res;
		}
	}

	return bddfalse;
}
