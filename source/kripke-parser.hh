#ifndef KRIPKE_PARSER_HH
# define KRIPKE_PARSER_HH

#include <boost/spirit/include/qi.hpp>
#include <boost/fusion/sequence/intrinsic/at_c.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/fusion/include/std_pair.hpp>
#include <boost/spirit/include/support_istream_iterator.hpp>

#include <bdd.h>
#include <fdd.h>

namespace spirit = boost::spirit;
namespace qi = boost::spirit::qi;
namespace phoenix = boost::phoenix;

struct Node {
	int num;
	std::vector<std::string> vars;
	std::vector<int> links;
};

BOOST_FUSION_ADAPT_STRUCT(
    Node,
	(int, num)
    (std::vector<std::string>, vars)
    (std::vector<int>, links)
)

class KripkeBDD {
	public:
		KripkeBDD() { }

		/**
		 * Initialize Buddy with two domains, depending on \param nb_nodes
		 */
		void init_bdd(int nb_nodes);

		/**
		 * Add a node to the labels and relation sets
		 */
		void insert_node(Node& n);

		/**
		 * Add a relation between \param a and \param b
		 */
		void set_relation(int a, int b);

		/**
		 * Attribute \param labels to \param node
		 */
		void set_labels(int node, std::vector<std::string>& labels);

		/**
		 * Simplify the graph by removing unreachable nodes (the 0 node is the
		 * only possible root) and the finite paths
		 */
		void simplify();

		bdd R_get() {
			return R_;
		}

		bdd L_get() {
			return L_;
		}

		std::map<std::string, int>& labels_get() {
			return labels_;
		}

		/**
		 * \return first bdd variable associated to the labels
		 */
		int offset_get() {
			return offset_;
		}

	private:
		KripkeBDD(const KripkeBDD&) { }
		
		/**
		 * Remove labels domains from \param L
		 * \return L without labels
		 */
		bdd remove_labels(const bdd L);
		
		std::map<std::string, int> labels_;
		bdd R_ = bddfalse;
		bdd L_ = bddfalse;
		int offset_ = 0;
		int max_ = 0;
};

template <typename Iterator>
struct KripkeFile : qi::grammar<Iterator> {
	KripkeFile (KripkeBDD& skripke);

	KripkeBDD& kripke;

	qi::rule<Iterator> file;
	qi::rule<Iterator, Node()> node;
	qi::rule<Iterator, std::vector<std::string>()> values;
	qi::rule<Iterator, std::vector<int>()> links;
	qi::rule<Iterator, std::string()> id;
};

#include "kripke-parser.hxx"

#endif
