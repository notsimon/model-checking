#include <iostream>
#include <string>
#include <fstream>

#include "kripke-parser.hh"
#include "ctl-eval.hh"
#include "ctl-parser.hh"

bool ctl_eval(KripkeBDD& kripke, const std::string& str) {
	std::string ctl(str);
	std::string::iterator beg = ctl.begin();
	std::string::iterator ed = ctl.end();

	Expr ctl_ast;
	CTLFormula<std::string::iterator> formula;
	bool parsed = qi::phrase_parse(
		beg, ed,
		formula,
		qi::space,
		ctl_ast
	);

	if (!parsed || beg != ed) {
		std::cerr << "An error occured while parsing this CTL formula: " << str << std::endl;
		bdd_done();
		exit(3);
	}

	CTLEval ev(kripke);
	bdd res = boost::apply_visitor(ev, ctl_ast);

	//std::cout << "bddset: " << bddset << res << std::endl;
	//std::cout << "fddset: " << fddset << res << std::endl;

	return (res & fdd_ithvar(0, 0)) != bddfalse;
}

int main (int argc, char** argv) {
	if (argc < 2) {
		std::cerr << "Usage: " << std::endl;
		std::cerr << argv[0] << " <Kripke file> <CTL formula>" << std::endl;
		std::cerr << argv[0] << " <Kripke file>" << std::endl;
		return 1;
	}

	// ===================
	// KRIPKE CONSTRUCTION
	// ===================

	// Parsing file
	std::ifstream in (argv[1]);
	in.unsetf(std::ios::skipws);

	spirit::istream_iterator begin(in);
	spirit::istream_iterator end;

	bdd_init(1000, 1000);
	bdd_gbc_hook(0);

	KripkeBDD kripke;
	KripkeFile<spirit::istream_iterator> kripke_grammar(kripke);

	bool parsed = qi::phrase_parse(
		begin, end,
		kripke_grammar,
		qi::space
	);

	kripke.simplify();
	//std::cout << "L = " << fddset << kripke.L_get() << std::endl;
	//std::cout << "R = " << fddset << kripke.R_get() << std::endl;

	if (!parsed || begin != end) {
		std::cerr << "An error occured while parsing the kripke file" << std::endl;
		bdd_done();
		return 2;
	}

	// ==============
	// CTL EVALUATION
	// ==============


	if (argc > 2) { // parse the command line argument
		if (ctl_eval(kripke, std::string(argv[2])))
			std::cout << "true" << std::endl;
		else
			std::cout << "false" << std::endl;
	}
	else { // parse stdin lines
		std::string str;
		while (!std::cin.eof()) {
			std::getline(std::cin, str);

			if (str.size() < 1) continue;

			if (ctl_eval(kripke, str))
				std::cout << "true" << std::endl;
			else
				std::cout << "false" << std::endl;
		}
	}

	bdd_done();
	return 0;

}
