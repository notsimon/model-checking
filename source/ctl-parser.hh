#ifndef CTL_PARSER_HH
# define CTL_PARSER_HH

#include <string>
#include <iostream>
#include <utility>

#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>

#include <boost/fusion/include/std_pair.hpp>
#include <boost/fusion/include/adapt_struct.hpp>

#include "ctl-structs.hh"

namespace spirit = boost::spirit;
namespace qi = boost::spirit::qi;
namespace phoenix = boost::phoenix;
namespace fusion = boost::fusion;


// AST
BOOST_FUSION_ADAPT_STRUCT(
	UnaryOp,
	(UnarySymbol, op)
	(Expr, expr)
)

BOOST_FUSION_ADAPT_STRUCT(
	BinaryOp,
	(BinarySymbol, op)
	(Expr, left)
	(Expr, right)
)

template <typename Iterator>
struct CTLFormula : qi::grammar<Iterator, Expr(), qi::space_type> {
	// symbols parsers
	// unary operations
	struct UnarySymbolTable : qi::symbols<char, UnarySymbol> {
		UnarySymbolTable () {
			add("not", UnarySymbol::NOT)
			   ("ax", UnarySymbol::AX)
			   ("ex", UnarySymbol::EX)
			   ("af", UnarySymbol::AF)
			   ("ef", UnarySymbol::EF)
			   ("ag", UnarySymbol::AG)
			   ("eg", UnarySymbol::EG);
		}
	} unary_symbol = UnarySymbolTable();

	// binary operations
	struct BinarySymbolTable : qi::symbols<char, BinarySymbol> {
		BinarySymbolTable () {
			add("and", BinarySymbol::AND)
			   ("or", BinarySymbol::OR)
			   ("implies", BinarySymbol::IMPLIES)
			   ("au", BinarySymbol::AU)
			   ("eu", BinarySymbol::EU);
		}
	} binary_symbol = BinarySymbolTable();

	CTLFormula();

	// rules
	qi::rule<Iterator, Expr(), qi::space_type> expr;
	qi::rule<Iterator, UnaryOp(), qi::space_type> unary_op;
	qi::rule<Iterator, BinaryOp(), qi::space_type> binary_op;
	qi::rule<Iterator, std::string()> id;
};

# include "ctl-parser.hxx"

#endif
