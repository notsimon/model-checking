#ifndef CTL_STRUCTS_HH
# define CTL_STRUCTS_HH

#include <boost/variant/variant.hpp>
#include <boost/variant/recursive_variant.hpp>

// SYMBOLS

enum class UnarySymbol {
	NOT,
	AX,
	EX,
	AF,
	EF,
	AG,
	EG
};

enum class BinarySymbol {
	AND,
	OR,
	IMPLIES,
	AU,
	EU
};

// AST

struct UnaryOp;
struct BinaryOp;

typedef
    boost::variant<
      bool,
      std::string,
      boost::recursive_wrapper<UnaryOp>,
      boost::recursive_wrapper<BinaryOp>
    >
	Expr;

struct UnaryOp {
	UnarySymbol op;
	Expr expr;
};

struct BinaryOp {
	BinarySymbol op;
	Expr left;
	Expr right;
};

#endif
