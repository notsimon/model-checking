#ifndef CTL_EVAL_HH
# define CTL_EVAL_HH

# include <string>

# include "bdd.h"
# include "fdd.h"

# include "ctl-structs.hh"
# include "kripke-parser.hh"

struct CTLEval: boost::static_visitor<bdd> {
	public:
		/**
		 * Constructor
		 * Create a new \struct KripkeBDD evaluator for \param kripke
		 */
		CTLEval(KripkeBDD& kripke)
			: kripke_(kripke) {
			int vars[kripke_.labels_get().size()];
			int i = 0;
			for (auto& p: kripke_.labels_get()) {
				vars[i] = p.second;
				++i;
			}
			nodes_ = bdd_makeset(vars, kripke_.labels_get().size());
		}

		/**
		 * \return bddtrue or bddfalse
		 */
		bdd operator() (bool o);

		/**
		 * \return all nodes labeled by \param o
		 */
		bdd operator() (std::string& o);
		
		/**
		 * \return the bdd resulting of the evaluation of the
		 * CTL expression \param o
		 */
		bdd operator() (UnaryOp& o);

		/**
		 * \return the bdd resulting of the evaluation of the
		 * CTL expression \param o
		 */
		bdd operator() (BinaryOp& o);

	protected:
		bdd EX(bdd expr);
		bdd AX(bdd expr);
		bdd NOT(bdd expr);

		/**
		 * Return the nodes (domain 0) which satify \param expr
		 * \return a bdd with only the domain 0 
		 */
		bdd constraint(bdd expr) {
			return bdd_exist(bdd_exist(kripke_.L_get() & expr, nodes_), fdd_ithset(1));
		}

	private:
		KripkeBDD& kripke_;
		bdd nodes_;
};

#endif
