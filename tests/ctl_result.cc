#include <iostream>
#include <string>
#include <fstream>

#include <gtest/gtest.h>

#include "kripke-parser.hh"
#include "ctl-eval.hh"
#include "ctl-parser.hh"

bool ctl_eval(const std::string& fn, const std::string& ctl) {
	std::string sfn(WAF_PROJECT_PATH);
	std::ifstream in (sfn + "/" + fn);
	in.unsetf(std::ios::skipws);

	spirit::istream_iterator begin(in);
	spirit::istream_iterator end;

	bdd_init(1000, 1000);
	bdd_gbc_hook(0);

	KripkeBDD kripke;
	KripkeFile<spirit::istream_iterator> kripke_grammar(kripke);

	bool parsed = qi::phrase_parse(
		begin, end,
		kripke_grammar,
		qi::space
	);

	kripke.simplify();

	if (!parsed || begin != end) {
		bdd_done();
		return false;
	}

	Expr ctl_ast;
	std::string sctl(ctl);
	std::string::iterator beg = sctl.begin();
	std::string::iterator ed = sctl.end();
	CTLFormula<std::string::iterator> formula;
	bool parsedd = qi::phrase_parse(
		beg, ed,
		formula,
		qi::space,
		ctl_ast
	);

	if (!parsedd || beg != ed) {
		bdd_done();
		return false;
	}
	CTLEval ev(kripke);
	bdd res = boost::apply_visitor(ev, ctl_ast);
	bool fres = (res & fdd_ithvar(0, 0)) != bddfalse;
	bdd_done();

	return fres;
}

TEST(CTLEval, very_simple) {
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","a"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","b"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","c"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","not(a)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","not(c)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","and(a,b)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","or(a,c)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","eu(a,not(c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","ax(a)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","ax(and(b,c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","ex(c)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","au(a,b)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","au(ex(c),au(b,c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","and(a,or(b,c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","implies(a,b)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","au(a,implies(b,c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","eg(a)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","ag(a)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","ef(b)"));
	EXPECT_FALSE(ctl_eval("tests/kripke/very_simple","af(c)"));
}

TEST(CTLEval, simple) {
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "a"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "b"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "not(c)"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "and(and(a,b),not(c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/simple", "ex(and(and(b,a),c))"));
	EXPECT_FALSE(ctl_eval("tests/kripke/simple", "ax(and(and(a,b),c))"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "implies(a,b)"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "af(b)"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "ef(b)"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "eu(b,not(c))"));
	EXPECT_TRUE(ctl_eval("tests/kripke/simple", "au(b,not(c))"));
}
