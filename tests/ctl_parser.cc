#include <iostream>
#include <string>
#include <fstream>

#include <gtest/gtest.h>

#include "ctl-parser.hh"

bool ctl_parse (const std::string& str) {
	std::string::const_iterator beg = str.begin();
	std::string::const_iterator end = str.end();

	CTLFormula<std::string::const_iterator> formula;
	bool parsed = qi::phrase_parse(
		beg, end,
		formula,
		qi::space
	);

	return parsed && beg == end;
}

TEST(CTLParser, IgnoreSpaces) {
	EXPECT_TRUE(ctl_parse("and(foo, bar)"));
	EXPECT_TRUE(ctl_parse("implies(foo,       bar)"));
	EXPECT_TRUE(ctl_parse("or       (foo,bar)"));
	EXPECT_TRUE(ctl_parse("and   (       foo      , bar)"));
	EXPECT_TRUE(ctl_parse("       not	(foo)	"));
}

TEST(CTLParser, IgnoreCase) {
	EXPECT_TRUE(ctl_parse("AND(foo,bar)"));
	EXPECT_TRUE(ctl_parse("ANd(foo,BAR)"));
	EXPECT_TRUE(ctl_parse("OR(fAo,bar)"));
	EXPECT_TRUE(ctl_parse("not(BAR)"));
	EXPECT_TRUE(ctl_parse("eX(foo)"));
}

TEST(CTLParser, ParseError) {
	EXPECT_FALSE(ctl_parse("foo(foo)"));
	EXPECT_FALSE(ctl_parse("and(foo)"));
	EXPECT_FALSE(ctl_parse("eg(fo,bar)"));
	EXPECT_FALSE(ctl_parse("not(B A   R)"));
	EXPECT_FALSE(ctl_parse("eX(foo"));
}