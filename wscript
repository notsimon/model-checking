#! /usr/bin/env python

import os

APPNAME = 'model-checking'
VERSION = '1.0'

top = '.'
out = 'build'

def options(ctx):
	ctx.load('compiler_cxx unittest_gtest test')
	
	ctx.add_option(
		'-m', '--mode',
		action			= 'store',
		default			= 'release',
		help			= 'build mode, release or debug'
	)

	ctx.add_option(
		'-i', '--include',
		action			= 'store',
		metavar			= 'PATH',
		help			= 'add PATH to the include search paths'
	)

	ctx.add_option(
		'-l', '--library',
		action			= 'store',
		metavar 		= 'PATH',
		help 			= 'add PATH to the static library search paths'
	)

def configure(ctx):
	ctx.load('compiler_cxx unittest_gtest test')

	# add user defined paths
	if ctx.options.include is not None:
		ctx.env.append_unique('INCLUDES', os.path.realpath(ctx.options.include))

	if ctx.options.library is not None:
		ctx.env.append_unique('STLIBPATH', os.path.realpath(ctx.options.library))

	# load buddy
	ctx.check(
		uselib_store		= 'Buddy',
		features			= 'cxx cxxprogram',
		stlib				= 'bdd'
	)

def build(ctx):
	cxxflags = {
		'release': ['-O4', '-w', '-DNDEBUG'],
		'debug': ['-O0', '-W', '-Wall', '-g', '-ggdb']
	}
	ctx.env.append_unique('CXXFLAGS', cxxflags[ctx.options.mode])
	ctx.env.append_unique('CXXFLAGS', '-DWAF_PROJECT_PATH="' + os.path.realpath(top) + '"')

	ctx.program(
		target		= 'mc',
		source		= ['source/main.cc', 'source/kripke-parser.cc', \
					   'source/ctl-eval.cc'],
		use			= 'Buddy',

		cxxflags	= '-std=c++11'
	)

	# unit tests

	ctx.program(
		target		= 'test',
		features	= 'gtest',
		includes 	= 'source',
		cxxflags	= '-std=c++11',
		use			= 'Buddy',

		source		= ctx.path.ant_glob([
						'source/kripke-parser.cc', 'source/ctl-eval.cc', 'tests/*.cc'])
	)

def dist(ctx):
	ctx.files = ctx.path.ant_glob([
						'wscript', 'waf', 'README', 'AUTHORS', 'doxyfile', 'unittest_gtest.py', \
						'source/**.cc', 'source/**.hxx', 'source/**.hh',\
						'tests/kripke/**', 'tests/*.cc'
					])
